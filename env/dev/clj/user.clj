(ns user
  (:require [mount.core :as mount]
            quickpicks.core))

(defn start []
  (mount/start-without #'quickpicks.core/repl-server))

(defn stop []
  (mount/stop-except #'quickpicks.core/repl-server))

(defn restart []
  (stop)
  (start))


