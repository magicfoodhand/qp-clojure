FROM java:8-alpine
MAINTAINER Mitch McGinnis <you@inapinch.xyz>

ADD target/uberjar/quickpicks.jar /quickpicks/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/quickpicks/app.jar"]
