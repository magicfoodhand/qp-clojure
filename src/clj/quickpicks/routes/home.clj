(ns quickpicks.routes.home
  (:require [quickpicks.layout :as layout]
            [compojure.core :refer [defroutes GET]]
            [ring.util.http-response :as response]
            [clojure.string :as str]
            [quickpicks.lottery :as lottery]
            [clojure.java.io :as io]))

(defn home-page []
  (layout/render
    "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page []
  (layout/render "about.html"))

(defn lottery-page [lottery]
  (layout/render "lottery.html" {:lottery (lottery/handle-lottery lottery)}))
  
(defroutes home-routes
  (GET "/" [] (home-page))
  (GET "/about" [] (about-page))
  (GET "/lottery/:lottery" [lottery] (lottery-page lottery)))
