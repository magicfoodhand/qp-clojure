(ns quickpicks.lottery
  (:require [clojure.string :as str]))

(defn pb-first-split
  "Get the first element then split on space-like characters"
  [element]
  (remove empty? (str/split (first element) #"\s")))

(defn fix-pball-keys
  [full-pball]
  (concat [(str/join " " (take 2 full-pball))]
    (rest (rest full-pball))))

(defn pball-map
  [keys rows]
  (if (empty? rows)
    nil
    (concat [(zipmap keys (first rows))] (pball-map keys (rest rows)))))

(defn zip-pball
  [{lottery :lottery keys :keys rows :rows}]
  { :lottery lottery
    :results (pball-map keys rows)})

(defn parse-pball
  [contents]
  (zip-pball { :lottery "Powerball"
    :keys (fix-pball-keys
      (map first (distinct (re-seq #"([a-zA-Z]+\d?)" contents))))
    :rows (map
      pb-first-split (re-seq #"(\d+/\d+/\d+).*(\d\d).+(\d)" contents))}))

(defn parse-mmill
  [contents]
  {:lottery "Mega Millions"
    :keys contents
    :rows contents}
  )

(defn handle-lottery
  "Do something with this lottery string"
  [lottery]
  (case lottery
    "powerball"(parse-pball
      (slurp "http://www.powerball.com/powerball/winnums-text.txt"))
    "megamillions"(parse-mmill
      (slurp "http://www.calottery.com/sitecore/content/Miscellaneous/download-numbers/?GameName=mega-millions&Order=No"))
    "default"))
